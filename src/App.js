import React from 'react';
import './App.css';

function App() {
  const [counter, setCounter] = React.useState(0);

  const increment = () =>{
    setCounter(counter +1);
  }

  const decrement = () =>{
    setCounter(counter -1);
  }

  return (
   <>
      <p>{counter}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
   </>
  );
}

export default App;
